FLAG_PATH=$HOME/.local/state/gnome-better-icons
FLAG_FILE=$FLAG_PATH/.flag-initial-icons-set # This flag is set so this script runs only once per user

if [ ! -e $FLAG_FILE ]
then
    mkdir $FLAG_PATH
    touch $FLAG_FILE

    CURSOR_THEME='Polarnight-cursors'
    COLOR_SCHEME=$(gsettings get org.gnome.desktop.interface color-scheme)
    if echo $COLOR_SCHEME | grep -iq dark; # check if gnome is in dark mode
    then
        ICON_THEME='Sevi-dark'
    else
        ICON_THEME='Sevi'
    fi
   
    gsettings set org.gnome.desktop.interface cursor-theme $CURSOR_THEME
    gsettings set org.gnome.desktop.interface icon-theme $ICON_THEME
fi

