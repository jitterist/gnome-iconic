# gnome-iconic

## Bundled Cursor themes
- https://github.com/yeyushengfan258/ArcStarry-Cursors
- https://github.com/yeyushengfan258/Polarnight-Cursors
- https://github.com/varlesh/volantes-cursors
- https://github.com/yeyushengfan258/Win7OS-cursors

## Bundled Icon Themes
- https://github.com/vinceliuice/Fluent-icon-theme
- https://github.com/TaylanTatli/Sevi
- https://github.com/vinceliuice/McMojave-circle
- https://github.com/yeyushengfan258/Win11-icon-theme
