from xml.etree import ElementTree as ET
import sys

SCHEMA_FILE_NAME = '/usr/share/glib-2.0/schemas/org.gnome.desktop.interface.gschema.xml'
XML_TREE = ET.parse(SCHEMA_FILE_NAME)
XML_ROOT = XML_TREE.getroot()
SYSTEM_THEME = 'dark' if (len(sys.argv) > 1 and sys.argv[1] == '--dark-theme') else 'light'
ICON_THEME = 'Sevi-dark' if SYSTEM_THEME == 'dark' else 'Sevi'
CURSOR_THEME = 'Polarnight-cursors'


def set_schema_xml_value(xml_root, key_node_name, new_default_node_text):
    for subroot in xml_root.findall('schema'):
        for key_node in subroot.findall('key'):
            if key_node.get('name') == key_node_name:
                default_node = key_node.find('default')
                default_node.text = new_default_node_text
                return
                
                
set_schema_xml_value(XML_ROOT, 'icon-theme', ICON_THEME)
set_schema_xml_value(XML_ROOT, 'cursor-theme', CURSOR_THEME)
XML_TREE.write(SCHEMA_FILE_NAME)

