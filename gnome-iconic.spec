Name:           gnome-iconic
Version:        1.0.0
Release:        0%{?dist}
Summary:        Modern icon and cursor themes for the gnome desktop
License:        GPLv3+
URL:            https://www.gnome-look.org
BuildArch:      noarch

Requires:       gnome-shell

BuildRequires:  git
BuildRequires:  tar

%description
Modern icon and cursor themes for the gnome desktop. Restart required.

%prep
git clone https://gitlab.com/jitterist/gnome-iconic.git

%build
cd gnome-iconic
tar -xf cursor_themes.tar.xz
tar -xf icon_themes.tar.xz

%install
# Install icons
mkdir -p %{buildroot}/%{_datadir}/icons
cp -ra gnome-iconic/cursor_themes/. %{buildroot}/%{_datadir}/icons
cp -ra gnome-iconic/icon_themes/. %{buildroot}/%{_datadir}/icons
# Install schema override
mkdir -p %{buildroot}/etc/profile.d
cp -ra gnome-iconic/set-initial-better-icons.sh %{buildroot}/etc/profile.d

%files
%{_datadir}/icons/*
/etc/*

